from uuid import uuid4
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger
from django.http.response import BadHeaderError
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from django.views.decorators.http import require_POST, require_http_methods
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
#from scrapyd_api import ScrapydAPI
from django.shortcuts import render
from django.http import HttpRequest
from django.template import RequestContext
from datetime import datetime
from django.http import HttpResponse
from app.models import recipe
from django.core.paginator import Paginator
from forms import ContactForm
from forms import CreaterecipeForm
from django.core.mail import send_mail
from django.http.response import HttpResponseForbidden
from django.shortcuts import redirect

import json

def home(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/index.html',
        {
            'title':'Home Page',
            'year':datetime.now().year,
        }
    )

def contact(request):
    if request.method == 'GET':
            form = ContactForm()
    else:
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = "Wiadomosc od BookOfRecipes"
            from_email = form.cleaned_data['contact_email']
            message = form.cleaned_data['content']
            try:
                send_mail(subject, message, from_email, ['bartoszdylewski16@gmail.com'])
            except BadHeaderError:
                return HttpResponse('Invalid header found.')
            return redirect('home')
    return render(request, 'app/contact.html', {'form': form})


def about(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/about.html',
        {
            'title':'About',
            'message':'Your application description page.',
            'year':datetime.now().year,
        }
    )


def manage_recipes(request):
    """Method that allows to delete invalid recipes (by admin)"""
    if not request.user.has_perm('recipes.manage_recipes'):
        return HttpResponseForbidden('Forbidden')

    page = request.GET.get('page', 1)

    recipe_list = recipe.objects.all()
    paginator = Paginator(recipe_list, 30)

    try:
        recipe_list = paginator.page(page)
    except PageNotAnInteger:
        recipe_list = paginator.page(1)
    except EmptyPage:
        recipe_list = paginator.page(paginator.num_pages)

    next_page = int(page) + 1

    return render(
        request,
        'app/manage_recipes.html',
        {
            'recipes': recipe_list,
            'page': next_page
        }
    )

def remove_recipe(request):

    if not request.user.has_perm('recipes.manage_recipes'):
        return HttpResponseForbidden('Forbidden')

    if request.method == 'GET':
        idx = request.GET.get('id', None)
        current_page = request.GET.get('page', 1)

    recipe = recipe.objects.get(id=idx)
    recipe.delete()

    return redirect('manage_recipes')



@csrf_exempt
@require_http_methods(['POST', 'GET'])
def test(request):
    if request.method == 'POST':

        url = request.POST.get('url', None)

        if not url:
            return JsonResponse({'error': 'Missing  args'})

        if not is_valid_url(url):
            return JsonResponse({'error': 'URL is invalid'})


    if request.method == 'GET':

        url = request.GET.get('url', None)

        if not url:
            return JsonResponse({'error': 'Missing  args'})

        if not is_valid_url(url):
            return JsonResponse({'error': 'URL is invalid'})

    return HttpResponse({})


def create_recipe(request):
    """Renders the create recipe page."""
    if not request.user.has_perm('recipes.create_recipes'):
        return HttpResponseForbidden('Forbidden')

    if request.method == 'GET':
        form = CreaterecipeForm()
    else:
        form = CreaterecipeForm(request.POST)
        if form.is_valid():
            recipe = recipe()
            recipe.name = form.data['name']
            recipe.link = form.data['link']
            recipe.desc = form.data['desc']
            recipe.country = form.data['country']
            recipe.category = form.data['category']
            recipe.save()

    return render(request, 'app/create_recipe.html', {'form': form,'year':datetime.date.today().year})


@csrf_exempt
@require_http_methods(['GET'])
def api_recipes(request):

    """
        Bez problemu paginuje przez wszystkie recipes

        http://127.0.0.1:8000/api/recipes/?page=3
    """

    if request.method == 'GET':

        page = request.GET.get('page', 1)

        if not page:
            return JsonResponse({'status':"error", 'message': 'Missing page'})

        recipe_list = recipe.objects.all()
        paginator = Paginator(recipe_list, 30)

        try:
            recipe_list = paginator.page(page)
        except PageNotAnInteger:
            recipe_list = paginator.page(1)
        except EmptyPage:
            recipe_list = paginator.page(paginator.num_pages)

        return HttpResponse(json.dumps([item.get_json() for item in recipe_list.object_list]) , content_type='application/json')

    else:

        return JsonResponse({'status':"error", 'message': 'Not GET request'})

@csrf_exempt
@require_http_methods(['POST'])
def api_create_recipe(request):

    if request.method == 'POST':

        name = request.POST.get('name', 1)
        link = request.POST.get('link', 1)
        desc = request.POST.get('desc', 1)
        price = request.POST.get('country', 1)
        category = request.POST.get('category', 1)

        if not name:
            return JsonResponse({'status':"error", 'message': 'Missing name'})
        if not link:
            return JsonResponse({'status':"error", 'message': 'Missing link'})
        if not desc:
            return JsonResponse({'status':"error", 'message': 'Missing desc'})
        if not country:
            return JsonResponse({'status':"error", 'message': 'Missing country'})
        if not category:
            return JsonResponse({'status':"error", 'message': 'Missing category'})

        recipe = recipe()
        recipe.name = name
        recipe.link = link
        recipe.desc = desc
        recipe.country = country
        recipe.category = category
        recipe.save()

        return JsonResponse({'status': 'OK', 'message': "recipe created"})

    else:

        return JsonResponse({'status':"error", 'message': 'Not POST request'})
