$(document).ready(function () {

    $('.tooltipped').tooltip({ delay: 50 });


    // aktualna strona
    var currentPage = 1;
    var globalItemPerRow = 4;

    function createItem(item)
    {
        console.log(item)

        return `
        <div class="col-md-3">
            <div class="item-card">
                 <div class="item-title"> `+ item['name'] +` </div>
                 <div style="text-align: center; padding: 10px 0">
                     <img src="https://img.huffingtonpost.com/asset/593aef321d00001400cc21fb.jpg?ops=scalefit_720_noupscale" alt="Smiley face" height="100" width="100" style="margin-top:5px; margin-bottom: 5px;" >
                 </div>
                 <div class="item-body"> Category:  `+ item['category'] +` </div>
                 <div class="item-body"> Region: `+ item['country'] +` </div>
                 <div class="item-desc"> Description: `+ item['desc'] +` </div>
                 <a href="`+ item['link'] +`"><div class="item-goto"> Odwiedź stronę </div></a>
            </div>
        </div>
        `;
    }

    function fullfillRow(itemsPerRow, items) {

        var resultString = ''
        var numberOfRows = Math.floor(items.length / itemsPerRow)
        var currentItemProcessed = 0

        for (i = 0; i <= numberOfRows; i++) {

            resultString = resultString + '<div class="row" style="margin-bottom: 10px;">'

            for (j = 0; j < itemsPerRow; j++) {

                if (items[currentItemProcessed] !== void 0) {
                    resfunc = createItem(items[currentItemProcessed])
                    resultString = resultString + resfunc
                }

                currentItemProcessed = currentItemProcessed + 1

            }

            resultString = resultString + "</div>"
        }

        return resultString
    }

    // pobierz dowolna strone
    function getDataFromPage(desiredPage) {
        $.get("api/pleaces/", { page: desiredPage }).done(function (data) {

            var parsedItems=[]

            for (i = 0; i < data.length; i++) {
                var parsed = JSON.parse(data[i]);
                //console.log(parsed[0]['fields'])
                parsedItems.push(parsed[0]['fields'])
            }

            var mainElem = $("#main");

            mainElem.append(fullfillRow(globalItemPerRow, parsedItems))

        });
    }

    // pobierz nastepna strone
    function nextPage()
    {
        currentPage = currentPage + 1;
        getDataFromPage(currentPage)
    }

    // pobierz pierwsza strone po zaladowaniu sie aplikacji
    getDataFromPage(currentPage);

    $(window).on("scroll", function () {
        var scrollHeight = $(document).height();
        var scrollPosition = $(window).height() + $(window).scrollTop();
        if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
            nextPage()
        }
    });

});
