"""
Definition of models.
"""

from django.db import models
from django.core import serializers

class Recipe(models.Model):
    name = models.CharField(max_length=100)
    desc = models.CharField(max_length=300,default="foobar")
    link = models.CharField(max_length=100)
    region = models.CharField(max_length=100)
    category = models.CharField(max_length=100)

    def get_json(item):
        serialized_obj = serializers.serialize('json', [ item ])
        return serialized_obj
