from django.core.management.base import BaseCommand
from app.models import Deal
import csv

"""

This command allow to import data from csv to database
Example how to run this command:

python manage.py importdeals --filename=app/media-markt.csv

"""

class Command(BaseCommand):
    help = 'A description of your command'

    def add_arguments(self, parser):
        parser.add_argument(
            '--filename', dest='filename', required=True,
            help='The filename to process',
        )

    def handle(self, *args, **options):
        status=0
        filename = options['filename']
        with open(filename, 'rb') as f:
            reader = csv.reader(f)
            for row in reader:
                print(status)
                deal = Deal()
                deal.name = row[0]
                deal.link = row[1]
                deal.price_old = row[2]
                deal.price = row[3]
                deal.reason = ''
                deal.available = 'Yes'
                deal.category = row[1].split('/')[1]
                deal.save()
                status = status + 1