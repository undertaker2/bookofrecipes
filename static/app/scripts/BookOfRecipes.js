$(document).ready(function () {

    $('.tooltipped').tooltip({ delay: 50 });


    // aktualna strona
    var currentPage = 1;
    var globalItemPerRow = 5;

    function createItem(item)
    {
        console.log(item)

        return `
        <div class="col-md-2 item-card">
            <div class="item-picture"></div>
            <div class="item-title"> `+ item['name'] +` </div>
            <div class="item-body"> `+ item['category'] +` </div>
            <div class="item-price-old"> `+ item['price_old'] +` </div>
            <div class="item-price"> `+ item['price'] +` </div>
            <a href="`+ item['link'] +`"><div class="item-goto"> Idz do okazji </div></a>
        </div>
        `;
    }

    function fullfillRow(itemsPerRow, items) {

        var resultString = ''
        var numberOfRows = Math.floor(items.length / itemsPerRow)
        var currentItemProcessed = 0

        for (i = 0; i <= numberOfRows; i++) {

            resultString = resultString + '<div class="row">'

            for (j = 0; j < itemsPerRow; j++) {
                
                if (items[currentItemProcessed] !== void 0) {
                    resfunc = createItem(items[currentItemProcessed])
                    resultString = resultString + resfunc
                }

                currentItemProcessed = currentItemProcessed + 1

            }

            resultString = resultString + "</div>"
        }

        return resultString
    }

    // pobierz dowolna strone
    function getDataFromPage(desiredPage) {
        $.get("api/deals/", { page: desiredPage }).done(function (data) {

            var parsedItems=[]

            for (i = 0; i < data.length; i++) {
                var parsed = JSON.parse(data[i]);
                //console.log(parsed[0]['fields'])
                parsedItems.push(parsed[0]['fields'])
            }

            var mainElem = $("#main");

            mainElem.append(fullfillRow(globalItemPerRow, parsedItems))

        });
    }

    // pobierz nastepna strone
    function nextPage()
    {
        currentPage = currentPage + 1;
        getDataFromPage(currentPage)
    }

    // pobierz pierwsza strone po zaladowaniu sie aplikacji
    getDataFromPage(currentPage);

    $(window).on("scroll", function () {
        var scrollHeight = $(document).height();
        var scrollPosition = $(window).height() + $(window).scrollTop();
        if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
            nextPage()
        }
    });

});

