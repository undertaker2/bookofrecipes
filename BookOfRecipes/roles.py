from rolepermissions.roles import AbstractUserRole
from BookOfRecipes import roles

class Admin(AbstractUserRole):
    available_permissions = {
        'recipes.create_recipes': True,
        'recipes.add_recipes': True
    }

class Viewer(AbstractUserRole):
    available_permissions = {
        'recipes.create_recipes': True,
        'recipes.manage_recipes': False,
    }
