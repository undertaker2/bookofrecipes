"""
Definition of urls for BookOfRecipes.
"""

from datetime import datetime
from django.conf.urls import url
from django.conf.urls import include
from django.contrib import admin

import app.forms
import app.views
import django.contrib.auth.views

# Uncomment the next lines to enable the admin:
# from django.conf.urls import include
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = [
    # Examples:
    url(r'^$', app.views.home, name='home'),
    url(r'^contact$', app.views.contact, name='contact'),
    url(r'^about', app.views.about, name='about'),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^recipes/create', app.views.create_recipe, name='create_recipe'),
    url(r'^recipes/manage', app.views.manage_recipes, name='manage_recipes'),
    url(r'^recipes/remove', app.views.remove_recipe, name='remove_recipe'),
    url(r'^api/test/', app.views.test, name='test'),
    url(r'^api/recipes/', app.views.api_recipes, name='api_recipes'),


    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
]
